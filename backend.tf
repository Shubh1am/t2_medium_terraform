terraform {
  backend "s3" {
    bucket = "uniquenameforterraformbackend15-03-2024" # Replace with your actual S3 bucket name
    key    = "Gitlab/terraform.tfstate"
    region = "us-east-1"
  }
}