resource "aws_instance" "my-machine" {

  count = 3

  ami = var.ec2_ami
  instance_type = var.instance_type
  subnet_id = "subnet-053ed839f17ecd002"
  key_name = "project"
  vpc_security_group_ids = ["sg-01264f9d8b57a69d2"]
  #passkey_authentication = true
  
  
  #Storage=20GB
  root_block_device {
    volume_type = "gp2"  # General Purpose SSD for better performance
    volume_size = 20  # Size in Gigabytes
  }

#  #save the IP address as the output instance_ips.txt
#  provisioner "local-exec" {
#    command = "echo \"${aws_instance.my-machine[*].public_ip}\" > instance_ips.txt"
#  }

  
  tags = {
   Name = "ec2-${count.index}"
  }
}

