output "public_ip_addresses" {
  value = aws_instance.my-machine[*].public_ip

  description = "List of public IP addresses for all EC2 instances"
}

output "private_ip_addresses" {
  value = aws_instance.my-machine[*].private_ip

  description = "List of private IP addresses for all EC2 instances"
}