
variable "ec2_ami"{
   default = "ami-0f58b397bc5c1f2e8"
}

variable "instance_type"{
   default = "t2.medium"
}

variable "region" {
   default = "ap-south-1"
}

